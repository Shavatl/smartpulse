#include <SoftwareSerial.h> 
SoftwareSerial ModBluetooth(2, 3); // RX | TX 
float sumador;
int pulso;
void setup() 
{  
    ModBluetooth.begin(9600); 
    Serial.begin(9600);  
}

void loop() 
{
  pulso = analogRead(A5);
  sumador = pulso * (5.0/1024.0);
  String a = String(sumador);
  ModBluetooth.print(a + "#"); 
  Serial.println(a + "#");
  delay(1000);
  
}

