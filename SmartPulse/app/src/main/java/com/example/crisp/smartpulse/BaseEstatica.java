package com.example.crisp.smartpulse;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by crisp on 05/04/2018.
 */

public class BaseEstatica
{
    public static final String shared = "smartPulse.com";
    public static final String shared_estado = "smart.estado";
    public static final String shared_usuario = "smart.usuario";
    public static final String shared_nombre = "smart.nombre";
    public static final String shared_fecha = "smart.fecha";
    public static final String shared_sexo = "smart.sexo";
    public static final String shared_telefono = "smart.telefono";
    public static final String shared_sangre = "smart.sangre";
    public static final String shared_peso = "smart.peso";
    public static final String shared_altura = "smart.altura";


    public static void guardarBoleano(Context c,boolean b,String k)
    {
        SharedPreferences preferences = c.getSharedPreferences(shared,c.MODE_PRIVATE);
        preferences.edit().putBoolean(k,b).apply();
    }

    public static boolean obtenerBoleano(Context c, String k)
    {
        SharedPreferences preferences = c.getSharedPreferences(shared,c.MODE_PRIVATE);
        return preferences.getBoolean(k,false);
    }

    public static void gardarUsuario(Context c,String b,String k)
    {
        SharedPreferences preferences = c.getSharedPreferences(shared,c.MODE_PRIVATE);
        preferences.edit().putString(k,b).apply();
    }

    public static String obtenerUsuario(Context c,String k)
    {
        SharedPreferences preferences = c.getSharedPreferences(shared,c.MODE_PRIVATE);
        return preferences.getString(k,"");
    }

    public static void gardarNombre(Context c,String b,String k)
    {
        SharedPreferences preferences = c.getSharedPreferences(shared,c.MODE_PRIVATE);
        preferences.edit().putString(k,b).apply();
    }

    public static String obtenerNombre(Context c, String k)
    {
        SharedPreferences preferences = c.getSharedPreferences(shared,c.MODE_PRIVATE);
        return preferences.getString(k,"");
    }

    public static void gardarFecha(Context c,String b,String k)
    {
        SharedPreferences preferences = c.getSharedPreferences(shared,c.MODE_PRIVATE);
        preferences.edit().putString(k,b).apply();
    }

    public static String obtenerFecha(Context c,String k)
    {
        SharedPreferences preferences = c.getSharedPreferences(shared,c.MODE_PRIVATE);
        return preferences.getString(k,"");
    }

    public static void gardarSexo(Context c,String b,String k)
    {
        SharedPreferences preferences = c.getSharedPreferences(shared,c.MODE_PRIVATE);
        preferences.edit().putString(k,b).apply();
    }

    public static String obtenerSexo(Context c,String k)
    {
        SharedPreferences preferences = c.getSharedPreferences(shared,c.MODE_PRIVATE);
        return preferences.getString(k,"");
    }

    public static void gardarTelefono(Context c,String b,String k)
    {
        SharedPreferences preferences = c.getSharedPreferences(shared,c.MODE_PRIVATE);
        preferences.edit().putString(k,b).apply();
    }

    public static String obtenerTelefono(Context c,String k)
    {
        SharedPreferences preferences = c.getSharedPreferences(shared,c.MODE_PRIVATE);
        return preferences.getString(k,"");
    }

    public static void gardarSangre(Context c,String b,String k)
    {
        SharedPreferences preferences = c.getSharedPreferences(shared,c.MODE_PRIVATE);
        preferences.edit().putString(k,b).apply();
    }

    public static String obtenerSangre(Context c,String k)
    {
        SharedPreferences preferences = c.getSharedPreferences(shared,c.MODE_PRIVATE);
        return preferences.getString(k,"");
    }

    public static void gardarPeso(Context c,String b,String k)
    {
        SharedPreferences preferences = c.getSharedPreferences(shared,c.MODE_PRIVATE);
        preferences.edit().putString(k,b).apply();
    }

    public static String obtenerPeso(Context c,String k)
    {
        SharedPreferences preferences = c.getSharedPreferences(shared,c.MODE_PRIVATE);
        return preferences.getString(k,"");
    }

    public static void gardarAltura(Context c,String b,String k)
    {
        SharedPreferences preferences = c.getSharedPreferences(shared,c.MODE_PRIVATE);
        preferences.edit().putString(k,b).apply();
    }

    public static String obtenerAltura(Context c,String k)
    {
        SharedPreferences preferences = c.getSharedPreferences(shared,c.MODE_PRIVATE);
        return preferences.getString(k,"");
    }
}
