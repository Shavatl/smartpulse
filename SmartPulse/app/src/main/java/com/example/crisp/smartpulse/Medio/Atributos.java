package com.example.crisp.smartpulse.Medio;

/**
 * Created by crisp on 07/04/2018.
 */

public class Atributos
{
    private String Nombre;
    private String DireccionMac;

    public Atributos()
    {

    }

    public String getNombre()
    {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getDireccionMac() {
        return DireccionMac;
    }

    public void setDireccionMac(String direccionMac)
    {
        DireccionMac = direccionMac;
    }
}
