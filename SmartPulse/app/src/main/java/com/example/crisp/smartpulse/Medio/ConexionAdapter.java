package com.example.crisp.smartpulse.Medio;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.provider.MediaStore;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.crisp.smartpulse.Final.Graficar;
import com.example.crisp.smartpulse.R;

import java.util.List;

/**
 * Created by crisp on 07/04/2018.
 */

public class ConexionAdapter extends RecyclerView.Adapter<ConexionAdapter.HolderConexion>
{

    private List<Atributos> atributosList;
    Context context;
    public ConexionAdapter(List<Atributos> atributosList, Context context)
    {
        this.atributosList = atributosList;
        this.context = context;

    }

    @Override
    public HolderConexion onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cam_conexion,parent,false);
        return new ConexionAdapter.HolderConexion(v);
    }

    @Override
    public void onBindViewHolder(HolderConexion holder, final int position)
    {

        holder.nombre.setText(atributosList.get(position).getNombre());
        holder.direccion.setText(atributosList.get(position).getDireccionMac());
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent i = new Intent(context, Graficar.class);
                i.putExtra("key_receptor",atributosList.get(position).getDireccionMac());
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return atributosList.size();
    }

    public class HolderConexion extends RecyclerView.ViewHolder {
        CardView cardView;
        TextView nombre;
        TextView direccion;

        public HolderConexion(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.cardV);
            nombre = (TextView) itemView.findViewById(R.id.NombreDispositivo);
            direccion = (TextView) itemView.findViewById(R.id.DireccionMAC);
        }
    }
}
