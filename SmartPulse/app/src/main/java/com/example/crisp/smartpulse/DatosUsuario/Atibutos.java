package com.example.crisp.smartpulse.DatosUsuario;

/**
 * Created by crisp on 07/04/2018.
 */

public class Atibutos
{
    private String Medicion;
    private String Hora;

    public Atibutos()
    {

    }



    public String getMedicion() {
        return Medicion;
    }

    public void setMedicion(String medicion) {
        Medicion = medicion;
    }

    public String getHora() {
        return Hora;
    }

    public void setHora(String hora) {
        Hora = hora;
    }
}
