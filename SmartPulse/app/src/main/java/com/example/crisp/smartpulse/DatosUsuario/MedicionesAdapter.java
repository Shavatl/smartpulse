package com.example.crisp.smartpulse.DatosUsuario;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.crisp.smartpulse.R;

import java.util.List;

/**
 * Created by crisp on 07/04/2018.
 */

public class MedicionesAdapter extends RecyclerView.Adapter<MedicionesAdapter.HolderMediciones>
{
    private List<Atibutos> atributosList;
    Context context;
    public MedicionesAdapter(List<Atibutos> atributosList, Context context)
    {
        this.atributosList = atributosList;
        this.context = context;

    }

    @Override
    public HolderMediciones onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cam_medicion,parent,false);
        return new MedicionesAdapter.HolderMediciones(v);
    }

    @Override
    public void onBindViewHolder(HolderMediciones holder, final int position)
    {
        holder.medicion.setText(atributosList.get(position).getMedicion());
        holder.hora.setText(atributosList.get(position).getHora());
    }

    @Override
    public int getItemCount() {
        return atributosList.size();
    }

    public class HolderMediciones extends RecyclerView.ViewHolder {

        TextView nombre;
        TextView medicion;
        TextView hora;

        public HolderMediciones(View itemView) {
            super(itemView);
            medicion = (TextView) itemView.findViewById(R.id.medictable);
            hora = (TextView) itemView.findViewById(R.id.medicTime);
        }
    }

}
