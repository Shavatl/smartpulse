package com.example.crisp.smartpulse.Medio;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.example.crisp.smartpulse.BaseEstatica;
import com.example.crisp.smartpulse.DatosUsuario.Datos;
import com.example.crisp.smartpulse.DatosUsuario.Mediciones;
import com.example.crisp.smartpulse.Inicio.Login;
import com.example.crisp.smartpulse.R;
import com.hitomi.cmlibrary.CircleMenu;
import com.hitomi.cmlibrary.OnMenuSelectedListener;
import com.hitomi.cmlibrary.OnMenuStatusChangeListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class Conexion extends AppCompatActivity {

    private RecyclerView rv;
    private CardView vinculados;

    private List<Atributos> atributosList;
    private ConexionAdapter adapter;

    private BluetoothAdapter myBluetooth = null;
    private Set<BluetoothDevice> dispVinculados;

    CircleMenu circleMenu;
    MediaPlayer mp;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conexion);

        circleMenu = (CircleMenu) findViewById(R.id.circle_menu);
        rv = (RecyclerView) findViewById(R.id.dispositivos);
        vinculados = (CardView) findViewById(R.id.vinculacion);
        LinearLayoutManager lm = new LinearLayoutManager(this);
        rv.setLayoutManager(lm);
        mp = MediaPlayer.create(this, R.raw.click);

        circleMenu.setMainMenu(Color.parseColor("#CDCDCD"),
                R.mipmap.icon_menu, R.mipmap.icon_cancel);
        circleMenu.addSubMenu(Color.parseColor("#258CFF"), R.mipmap.icon_home)
                .addSubMenu(Color.parseColor("#FF4B32"), R.mipmap.icon_notify)
                .addSubMenu(Color.parseColor("#2A3c58"), R.drawable.ic_action_user_blanco)
                .addSubMenu(Color.parseColor("#3b63a1"), R.drawable.ic_action_bargraph);

        circleMenu.setOnMenuSelectedListener(new OnMenuSelectedListener() {
            @Override
            public void onMenuSelected(int index) {
                mp.start();
                switch (index) {
                    case 0:
                        new android.os.Handler().postDelayed(
                                new Runnable() {
                                    public void run() {
                                        BaseEstatica.guardarBoleano(Conexion.this,false,BaseEstatica.shared_estado);
                                        Intent i = new Intent(Conexion.this, Login.class);
                                        mp.start();
                                        startActivity(i);
                                        finish();
                                        overridePendingTransition(R.anim.zoom_back_in,R.anim.zoom_back_out);
                                    }
                                }, 850);
                        break;
                    case 2:
                        new android.os.Handler().postDelayed(
                                new Runnable() {
                                    public void run() {
                                        Intent i = new Intent(Conexion.this, Datos.class);
                                        mp.start();
                                        startActivity(i);
                                        overridePendingTransition(R.anim.zoom_forward_in,R.anim.zoom_forward_out);
                                    }
                                }, 850);
                        break;
                    case 3:
                        new android.os.Handler().postDelayed(
                                new Runnable() {
                                    public void run() {
                                        Intent i = new Intent(Conexion.this, Mediciones.class);
                                        mp.start();
                                        startActivity(i);
                                        overridePendingTransition(R.anim.zoom_forward_in,R.anim.zoom_forward_out);
                                    }
                                }, 850);
                        break;
                }
            }
        }

        );

        circleMenu.setOnMenuStatusChangeListener(new OnMenuStatusChangeListener() {
            @Override
            public void onMenuOpened() {
            }
            @Override
            public void onMenuClosed() {
            }
        }
        );

        myBluetooth = BluetoothAdapter.getDefaultAdapter();

        if(myBluetooth == null)
        {
            Toast.makeText(getApplicationContext(), "Bluetooth no disponible", Toast.LENGTH_LONG).show();

            finish();
        }
        else if(!myBluetooth.isEnabled())
        {
            Intent turnBTon = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(turnBTon,1);
        }

        atributosList = new ArrayList<>();

        adapter = new ConexionAdapter(atributosList,Conexion.this);
        rv.setAdapter(adapter);

        vinculados.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                mp.start();
                dispositivosVinculados();


            }
        });
    }

    public void dispositivosVinculados()
    {
        dispVinculados = myBluetooth.getBondedDevices();

        if (dispVinculados.size()>0)
        {
            for(BluetoothDevice bt : dispVinculados)
            {
                agregarDispositivos(bt.getName(),bt.getAddress());
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(), "No se han encontrado dispositivos vinculados", Toast.LENGTH_LONG).show();
        }

    }

    public void agregarDispositivos(String nombre,String direccion)
    {
        Atributos atributos = new Atributos();
        atributos.setNombre(nombre);
        atributos.setDireccionMac(direccion);
        adapter.notifyDataSetChanged();
        atributosList.add(atributos);
    }


    @Override
    public void onBackPressed() {
        if (circleMenu.isOpened())
            circleMenu.closeMenu();
        else
            finish();
    }
}
