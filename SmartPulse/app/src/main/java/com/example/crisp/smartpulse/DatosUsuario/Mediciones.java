package com.example.crisp.smartpulse.DatosUsuario;

import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.crisp.smartpulse.BaseEstatica;
import com.example.crisp.smartpulse.Final.Graficar;
import com.example.crisp.smartpulse.Medio.Conexion;
import com.example.crisp.smartpulse.R;
import com.example.crisp.smartpulse.VolleyRP;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.ValueDependentColor;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static java.lang.Integer.parseInt;

public class Mediciones extends AppCompatActivity {

    private GraphView graph2;

    private RecyclerView rv;
    private MedicionesAdapter adapter;
    private Button oki;
    private TextView names;

    private String usuario="";
    private VolleyRP volley;
    private List<Atibutos> atributosList;
    private RequestQueue mRequest;
    MediaPlayer mp;

    private static String URL_Medicion = "https://crispinquintero08.000webhostapp.com/PHPSmartPulse/Mediciones_GETALL.php";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mediciones);

        volley = VolleyRP.getInstance(this);
        mRequest = volley.getRequestQueue();
        atributosList = new ArrayList<>();

        graph2 = (GraphView) findViewById(R.id.graph2);
        rv = (RecyclerView) findViewById(R.id.TableMedic);
        oki = (Button) findViewById(R.id.OKi);
        names = (TextView) findViewById(R.id.texnameuser);
        LinearLayoutManager lm = new LinearLayoutManager(this);
        rv.setLayoutManager(lm);

        mp = MediaPlayer.create(this, R.raw.click);

        usuario = BaseEstatica.obtenerUsuario(this, BaseEstatica.shared_usuario);
        MandarMedicion(usuario);

        adapter = new MedicionesAdapter(atributosList,Mediciones.this);
        rv.setAdapter(adapter);

        oki.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp.start();
                Intent i = new Intent(Mediciones.this, Conexion.class);
                startActivity(i);
                finish();
                overridePendingTransition(R.anim.zoom_back_in,R.anim.zoom_back_out);
            }
        });


        graph2.getViewport().setScalable(true);


        graph2.getViewport().setScrollable(true);

        graph2.getViewport().setScalableY(true);

        graph2.getViewport().setScrollableY(true);
    }

    public void grafica(int r,String c)
    {
        int d = Integer.parseInt(c);
        BarGraphSeries<DataPoint> series = new BarGraphSeries<>(new DataPoint[] {
                new DataPoint(r, d)
        });
        graph2.addSeries(series);

        series.setValueDependentColor(new ValueDependentColor<DataPoint>() {
            @Override
            public int get(DataPoint data)
            {
                return Color.rgb((int) data.getX()*255/4, (int) Math.abs(data.getY()*255/6), 100);
            }
        });

        series.setSpacing(20);

        series.setDrawValuesOnTop(true);
        series.setValuesOnTopColor(Color.RED);

    }

    private void MandarMedicion(String d)
    {
        HashMap<String,String> hashMapToken = new HashMap<>();
        hashMapToken.put("usuario",d);

        String name = BaseEstatica.obtenerNombre(Mediciones.this, BaseEstatica.shared_nombre);
        String age = BaseEstatica.obtenerFecha(Mediciones.this, BaseEstatica.shared_fecha);
        String acortado = age.substring(age.length()-4,age.length());

        int edad1 = parseInt(acortado);
        edad1=2018-edad1;
        names.setText(name + "         " + Integer.toString(edad1)+" años");

        JsonObjectRequest solicitud = new JsonObjectRequest(Request.Method.POST,URL_Medicion,new JSONObject(hashMapToken), new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject datos)
            {
                try
                {
                    String TodosLosDatos = datos.getString("resultado");
                    JSONArray jsonArray = new JSONArray(TodosLosDatos);
                    for(int i=0;i<=jsonArray.length();i++)
                    {

                        JSONObject js = jsonArray.getJSONObject(i);
                        String fech = js.getString("fecha");
                        String pre = js.getString("presion");
                        agregarMediciones(pre+" BPM",fech);
                        grafica(i,pre);
                    }


                }
                catch (JSONException e)
                {

                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Toast.makeText(Mediciones.this,"Ocurrio un error", Toast.LENGTH_SHORT).show();
            }
        });
        VolleyRP.addToQueue(solicitud,mRequest,this,volley);


    }

    public void agregarMediciones(String medicion,String hora)
    {
        Atibutos atributos = new Atibutos();
        atributos.setMedicion(medicion);
        atributos.setHora(hora);
        atributosList.add(atributos);
        adapter.notifyDataSetChanged();
    }
}
