package com.example.crisp.smartpulse.DatosUsuario;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.crisp.smartpulse.BaseEstatica;
import com.example.crisp.smartpulse.Inicio.Login;
import com.example.crisp.smartpulse.Medio.Conexion;
import com.example.crisp.smartpulse.R;
import com.example.crisp.smartpulse.VolleyRP;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class Datos extends AppCompatActivity {


    private TextView usuario;
    private TextView nombre;
    private TextView años;
    private TextView celular;
    private TextView sexo;
    private TextView sangre;
    private TextView peso;
    private TextView altura;
    private Button OK;

    private VolleyRP volley;
    private RequestQueue mRequest;
    MediaPlayer mp;

    private static String URL_Medicion = "https://crispinquintero08.000webhostapp.com/PHPSmartPulse/Mediciones_GETALL.php";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datos);

        volley = VolleyRP.getInstance(this);
        mRequest = volley.getRequestQueue();

        usuario = (TextView) findViewById(R.id.userDate);
        nombre = (TextView) findViewById(R.id.namecomplet);
        años = (TextView) findViewById(R.id.fechaDate);
        celular = (TextView) findViewById(R.id.telefonoDate);
        sexo = (TextView) findViewById(R.id.masfem);
        sangre = (TextView) findViewById(R.id.dSangre);
        peso = (TextView) findViewById(R.id.dPeso);
        altura = (TextView) findViewById(R.id.dAltura);

        OK = (Button) findViewById(R.id.OK);
        mp = MediaPlayer.create(this, R.raw.click);

        usuario.setText(BaseEstatica.obtenerUsuario(Datos.this, BaseEstatica.shared_usuario));
        nombre.setText(BaseEstatica.obtenerNombre(Datos.this, BaseEstatica.shared_nombre));
        años.setText(BaseEstatica.obtenerFecha(Datos.this, BaseEstatica.shared_fecha));
        celular.setText(BaseEstatica.obtenerTelefono(Datos.this, BaseEstatica.shared_telefono));
        sexo.setText(BaseEstatica.obtenerSexo(Datos.this, BaseEstatica.shared_sexo));
        sangre.setText(BaseEstatica.obtenerSangre(Datos.this, BaseEstatica.shared_sangre));
        peso.setText(BaseEstatica.obtenerPeso(Datos.this, BaseEstatica.shared_peso)+" kg");
        altura.setText(BaseEstatica.obtenerAltura(Datos.this, BaseEstatica.shared_altura)+" cm");

        OK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Datos.this, Conexion.class);
                startActivity(i);
                mp.start();
                finish();
                overridePendingTransition(R.anim.zoom_back_in,R.anim.zoom_back_out);
            }
        });

    }
}
