package com.example.crisp.smartpulse.Inicio;

import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.crisp.smartpulse.BaseEstatica;
import com.example.crisp.smartpulse.Medio.Conexion;
import com.example.crisp.smartpulse.R;
import com.example.crisp.smartpulse.VolleyRP;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Login extends AppCompatActivity {

    private EditText usuario;
    private EditText pasword;
    private RadioButton sesion;
    private CardView ingresar;
    private FloatingActionButton home;

    private String User="";
    private String Password="";

    private VolleyRP volley;
    private RequestQueue mRequest;

    private boolean activado;
    MediaPlayer mp;

    private static String IP = "https://crispinquintero08.000webhostapp.com/PHPSmartPulse/Login_GETID.php?id=";
    private static String URL_Usuarios = "https://crispinquintero08.000webhostapp.com/PHPSmartPulse/Usuario_GETALL.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        volley = VolleyRP.getInstance(this);
        mRequest = volley.getRequestQueue();

        usuario = (EditText) findViewById(R.id.Etuser);
        pasword = (EditText) findViewById(R.id.Etpassword);
        sesion = (RadioButton) findViewById(R.id.rbSesion);
        ingresar = (CardView) findViewById(R.id.btIngresar);
        home=(FloatingActionButton) findViewById(R.id.home);
        mp = MediaPlayer.create(this, R.raw.click);

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent i = new Intent(Login.this, Menu.class);
                startActivity(i);
                mp.start();
                finish();
                overridePendingTransition(R.anim.right_in,R.anim.right_out);

            }
        });


        ingresar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                mp.start();
                verificacion(usuario.getText().toString(),pasword.getText().toString());

            }
        });

        activado = sesion.isChecked();

        sesion.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(activado)
                {
                    sesion.setChecked(false);
                }
                activado = sesion.isChecked();

            }
        });
    }

    public void verificacion(String user, String password)
    {
        User = user;
        Password = password;
        requestJSON(IP+user);
    }

    public void requestJSON(String URL)
    {
        JsonObjectRequest requets = new JsonObjectRequest(URL, null, new Response.Listener<JSONObject>()
        {

            @Override
            public void onResponse(JSONObject dato)
            {
                checkLoging(dato);

            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Toast.makeText(Login.this,"Ocurrio un error.",Toast.LENGTH_SHORT).show();

            }
        });
        VolleyRP.addToQueue(requets,mRequest,this,volley);
    }

    public void checkLoging(JSONObject dato)
    {
        try
        {
            String estado = dato.getString("resultado");
            if(estado.equals("CC"))
            {
                JSONObject Jsondatos = new JSONObject(dato.getString("datos"));
                String usuario = Jsondatos.getString("id");
                String contraseña = Jsondatos.getString("Password");

                if(usuario.equals(User) && contraseña.equals(Password))
                {
                    BaseEstatica.guardarBoleano(Login.this,sesion.isChecked(),BaseEstatica.shared_estado);
                    BaseEstatica.gardarUsuario(Login.this,User,BaseEstatica.shared_usuario);
                    requestJSON();
                    login();
                }
                else
                {
                    Toast.makeText(this,"Contraseña Incorrecta",Toast.LENGTH_SHORT).show();

                }

            }
            else
            {
                Toast.makeText(this,"No existe el usuario",Toast.LENGTH_SHORT).show();
            }
        }
        catch (JSONException e)
        {

        }

    }

    public void AgregarDatos(String nombre,String fecha,String telefono,String sangre,String peso,String altura,String sexo)
    {
        BaseEstatica.gardarNombre(this,nombre,BaseEstatica.shared_nombre);
        BaseEstatica.gardarFecha(this,fecha,BaseEstatica.shared_fecha);
        BaseEstatica.gardarTelefono(this,telefono,BaseEstatica.shared_telefono);
        BaseEstatica.gardarSexo(this,sexo,BaseEstatica.shared_sexo);
        BaseEstatica.gardarSangre(this,sangre,BaseEstatica.shared_sangre);
        BaseEstatica.gardarPeso(this,peso,BaseEstatica.shared_peso);
        BaseEstatica.gardarAltura(this,altura,BaseEstatica.shared_altura);

    }

    public void requestJSON()
    {
        JsonObjectRequest requets = new JsonObjectRequest(URL_Usuarios, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject dato)
            {
                try
                {
                    String TodosLosDatos = dato.getString("resultado");
                    JSONArray jsonArray = new JSONArray(TodosLosDatos);
                    String NuestroUsuario = BaseEstatica.obtenerUsuario(Login.this,BaseEstatica.shared_usuario);
                    for(int i=0;i<jsonArray.length();i++)
                    {
                        JSONObject js = jsonArray.getJSONObject(i);
                        if(NuestroUsuario.equals(js.getString("id")))
                        {
                            AgregarDatos(js.getString("nombre")+" "+ js.getString("apellidos"),js.getString("fecha_de_nacimiento"),js.getString("telefono"),js.getString("T_Sangre"),
                                    js.getString("Peso"),js.getString("Altura"),js.getString("genero"));
                        }
                    }
                }
                catch (JSONException e)
                {
                    Toast.makeText(Login.this,"Ocurrio un error.",Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Toast.makeText(Login.this,"Ocurrio un error.",Toast.LENGTH_SHORT).show();

            }
        });
        VolleyRP.addToQueue(requets,mRequest,Login.this,volley);
    }

    public void login()
    {

        final ProgressDialog progressDialog = new ProgressDialog(this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Autenticando...");
        progressDialog.show();

        // TODO: Implement your own authentication logic here.

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {

                        Intent i = new Intent(Login.this, Conexion.class);
                        startActivity(i);
                        finish();
                        progressDialog.dismiss();
                        overridePendingTransition(R.anim.zoom_forward_in,R.anim.zoom_forward_out);
                    }
                }, 3000);
    }

}
