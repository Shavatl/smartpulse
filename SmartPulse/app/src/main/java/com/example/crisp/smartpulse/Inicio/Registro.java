package com.example.crisp.smartpulse.Inicio;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.crisp.smartpulse.R;
import com.example.crisp.smartpulse.VolleyRP;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class Registro extends AppCompatActivity {
    private EditText user;
    private EditText password;
    private EditText password2;
    private EditText nombre;
    private EditText apellidos;
    private EditText dia;
    private EditText mes;
    private EditText año;
    private EditText telefono;
    private EditText sangre;
    private EditText peso;
    private EditText altura;
    private CardView registro;
    private RadioButton rdHombre;
    private RadioButton rdMujer;
    private FloatingActionButton home;
    private LinearLayout rv;

    private VolleyRP volley;
    private RequestQueue mRequest;

    MediaPlayer mp;

    private static final String IP_REGISTRAR = "https://crispinquintero08.000webhostapp.com/PHPSmartPulse/Registro_INSERT.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);


        volley = VolleyRP.getInstance(this);
        mRequest = volley.getRequestQueue();
        mp = MediaPlayer.create(this, R.raw.click);

        //home = (CardView) findViewById(R.id.homes);
        user = (EditText) findViewById(R.id.user);
        password = (EditText) findViewById(R.id.passwor);
        password2 = (EditText) findViewById(R.id.passwor2);
        nombre = (EditText) findViewById(R.id.nombre);
        apellidos = (EditText) findViewById(R.id.apellidos);
        dia = (EditText) findViewById(R.id.dia);
        mes = (EditText) findViewById(R.id.mes);
        año = (EditText) findViewById(R.id.año);
        telefono = (EditText) findViewById(R.id.telefonoRegistro);
        sangre = (EditText) findViewById(R.id.rSangre);
        peso = (EditText) findViewById(R.id.rPeso);
        altura = (EditText) findViewById(R.id.rAltura);

        rv = (LinearLayout) findViewById(R.id.rv);

        rdHombre = (RadioButton) findViewById(R.id.rdHombre);
        rdMujer = (RadioButton) findViewById(R.id.rdMujer);

        registro = (CardView) findViewById(R.id.btRegistro);

        rdHombre.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                rdMujer.setChecked(false);
            }
        });

        rdMujer.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                rdHombre.setChecked(false);
            }
        });

        LinearLayoutManager lm = new LinearLayoutManager(this);
        lm.setStackFromEnd(true);

        home=(FloatingActionButton) findViewById(R.id.homeregistro);

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent i = new Intent(Registro.this, Menu.class);
                startActivity(i);
                mp.start();
                finish();
                overridePendingTransition(R.anim.left_in, R.anim.left_out);

            }
        });

        registro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String genero = "";

                mp.start();
                if (rdHombre.isChecked()) genero = "Masculino";
                else if (rdMujer.isChecked()) genero = "Femenino";

                if(!getStringET(user).isEmpty() && !getStringET(password).isEmpty() && !getStringET(password2).isEmpty()&& !getStringET(nombre).isEmpty() &&
                        !getStringET(apellidos).isEmpty() && !getStringET(dia).isEmpty() && !getStringET(mes).isEmpty() &&
                        !getStringET(año).isEmpty() && !getStringET(telefono).isEmpty() && !getStringET(sangre).isEmpty() && !getStringET(peso).isEmpty()
                        && !getStringET(altura).isEmpty())
                {
                    if(getStringET(password).equals(getStringET(password2)))
                    {
                        if(getIntET(dia)<=31 && getIntET(dia)>=1 && getIntET(mes)<=12 && getIntET(mes)>=1 &&
                                getIntET(año)<=2018 && getIntET(año)>=1905)
                        {
                            registrarWebService(
                                    getStringET(user).trim(),
                                    getStringET(password).trim(),
                                    getStringET(nombre).trim(),
                                    getStringET(apellidos).trim(),
                                    getStringET(dia).trim() + "/" + getStringET(mes).trim() + "/" + getStringET(año).trim(),
                                    getStringET(telefono).trim(),
                                    getStringET(sangre).trim(),
                                    getStringET(peso).trim(),
                                    getStringET(altura).trim(),
                                    genero);
                        }
                        else
                        {
                            Toast.makeText(Registro.this,"La fecha no es correcta",Toast.LENGTH_SHORT).show();
                            año.setText("");
                            dia.setText("");
                            mes.setText("");
                        }
                    }
                    else
                    {
                        Toast.makeText(Registro.this,"La contraseña no es correcta",Toast.LENGTH_SHORT).show();
                        password.setText("");
                        password2.setText("");
                    }

                }
                else
                {
                    Toast.makeText(Registro.this,"Tiene campos en blanco",Toast.LENGTH_SHORT).show();

                }
            }
        });
    }

    private void registrarWebService(String usuario,String contraseña,String nombre,String apellido,String fechaNacimiento,String numero,String san,String pes,String altur, String genero){
        HashMap<String,String> hashMapToken = new HashMap<>();
        hashMapToken.put("id",usuario);
        hashMapToken.put("nombre",nombre);
        hashMapToken.put("apellidos",apellido);
        hashMapToken.put("fecha_nacimiento",fechaNacimiento);
        hashMapToken.put("genero",genero);
        hashMapToken.put("telefono",numero);
        hashMapToken.put("sangre",san);
        hashMapToken.put("peso",pes);
        hashMapToken.put("altura",altur);
        hashMapToken.put("password",contraseña);

        JsonObjectRequest solicitud = new JsonObjectRequest(Request.Method.POST,IP_REGISTRAR,new JSONObject(hashMapToken), new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject datos) {
                try {
                    String estado = datos.getString("resultado");
                    if(estado.equalsIgnoreCase("El usuario se registro correctamente")){
                        Toast.makeText(Registro.this,estado, Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(Registro.this, Login.class);
                        startActivity(i);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                    }else{
                        Toast.makeText(Registro.this,estado, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    Toast.makeText(Registro.this,"No se pudo registrar",Toast.LENGTH_SHORT).show();
                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Registro.this,"No se pudo registrar",Toast.LENGTH_SHORT).show();
            }
        });
        VolleyRP.addToQueue(solicitud,mRequest,this,volley);
    }


    private String getStringET(EditText e)
    {
        return e.getText().toString();
    }

    private Integer getIntET(EditText r)
    {
        int nInt;
        return nInt = new Integer(r.getText().toString()).intValue();
    }

}
