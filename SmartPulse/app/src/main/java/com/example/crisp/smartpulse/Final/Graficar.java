package com.example.crisp.smartpulse.Final;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.crisp.smartpulse.BaseEstatica;
import com.example.crisp.smartpulse.Inicio.Login;
import com.example.crisp.smartpulse.Medio.Conexion;
import com.example.crisp.smartpulse.R;
import com.example.crisp.smartpulse.VolleyRP;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.UUID;

import static java.lang.Integer.parseInt;

public class Graficar extends AppCompatActivity {


    private ConnectedThread MyConexionBT;
    private BluetoothAdapter btAdapter = null;
    private BluetoothSocket btSocket = null;
    private StringBuilder DataStringIN = new StringBuilder();
    private String address = null;

    Handler bluetoothIn;
    final int handlerState = 0;
    private TextView pulsa;
    private Button guardar;
    private Button regresar;
    private LinearLayout mostrar;
    private LinearLayout ocultar;
    private EditText dato;
    private Button listo;
    private int evaluar=0;


    private TextView nombre;
    private TextView edad;
    private TextView sexo;

    private String usuario="";
    private String enviar_medicion = "";
    private static final String IP_medicion = "https://crispinquintero08.000webhostapp.com/PHPSmartPulse/Enviar_Medicion.php";

    private VolleyRP volley;
    private RequestQueue mRequest;

    MediaPlayer mp;

    static final UUID myUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");


    @SuppressLint("HandlerLeak")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graficar);

        volley = VolleyRP.getInstance(this);
        mRequest = volley.getRequestQueue();
        mp = MediaPlayer.create(this, R.raw.click);

        usuario = BaseEstatica.obtenerUsuario(this, BaseEstatica.shared_usuario);

        pulsa = (TextView) findViewById(R.id.pulsacion);
        guardar = (Button) findViewById(R.id.Guardar);

        nombre = (TextView) findViewById(R.id.NombreUsuario);
        edad = (TextView) findViewById(R.id.Edad);
        sexo = (TextView) findViewById(R.id.Sexo);

        mostrar = (LinearLayout) findViewById(R.id.mostrar);
        ocultar = (LinearLayout) findViewById(R.id.ocultar);
        dato = (EditText) findViewById(R.id.datoGuardar);
        listo = (Button) findViewById(R.id.Verific);
        regresar = (Button) findViewById(R.id.returner);

        regresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Graficar.this, Conexion.class);
                mp.start();
                startActivity(i);
                finish();
                overridePendingTransition(R.anim.zoom_back_in,R.anim.zoom_back_out);
            }
        });

        Intent i = getIntent();
        Bundle bundle = i.getExtras();
        address = bundle.getString("key_receptor");

        String name = BaseEstatica.obtenerNombre(Graficar.this, BaseEstatica.shared_nombre);
        String age = BaseEstatica.obtenerFecha(Graficar.this, BaseEstatica.shared_fecha);
        String sex = BaseEstatica.obtenerSexo(Graficar.this, BaseEstatica.shared_sexo);

        String acortado = age.substring(age.length()-4,age.length());

        int edad1 = parseInt(acortado);
        edad1=2018-edad1;

        nombre.setText(name);
        edad.setText("Edad: "+ Integer.toString(edad1)+" años");
        sexo.setText("Sexo: "+sex);

        bluetoothIn = new Handler() {
            public void handleMessage(android.os.Message msg) {
                if (msg.what == handlerState) {
                    String readMessage = (String) msg.obj;
                    DataStringIN.append(readMessage);

                    int endOfLineIndex = DataStringIN.indexOf("#");
                    if (endOfLineIndex > 0)
                    {

                        String dataInPrint = DataStringIN.substring(2, endOfLineIndex);
                        pulsa.setText(dataInPrint + " BPM");
                        DataStringIN.delete(0, DataStringIN.length());

                    }
                }
            }
        };

        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                mp.start();
                mostrar.setVisibility(View.VISIBLE);
                ocultar.setVisibility(View.GONE);
                LottieAnimationView animation = (LottieAnimationView) findViewById(R.id.animationlove);
                animation.playAnimation();


            }
        });

        listo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!getStringET(dato).isEmpty())
                {
                    enviar_medicion = dato.getText().toString().trim();
                    MandarMedicion(usuario,enviar_medicion);
                    mp.start();
                    mostrar.setVisibility(View.GONE);
                    ocultar.setVisibility(View.VISIBLE);

                }
                else
                {
                    Toast.makeText(Graficar.this,"Tiene el campo en blanco",Toast.LENGTH_SHORT).show();

                }

            }
        });

        btAdapter = BluetoothAdapter.getDefaultAdapter();
        VerificarEstadoBT();

    }

    private BluetoothSocket createBluetoothSocket(BluetoothDevice device) throws IOException {
        //crea un conexion de salida segura para el dispositivo
        //usando el servicio UUID
        return device.createRfcommSocketToServiceRecord(myUUID);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        BluetoothDevice device = btAdapter.getRemoteDevice(address);
        try
        {
            btSocket = createBluetoothSocket(device);
        } catch (IOException e) {
            Toast.makeText(getBaseContext(), "Ocurrio un error", Toast.LENGTH_LONG).show();
        }
        // Establece la conexión con el socket Bluetooth.
        try
        {
            btSocket.connect();
        }
        catch (IOException e)
        {
            try
            {
                btSocket.close();
            }
            catch (IOException e2) {}
        }
        MyConexionBT = new ConnectedThread(btSocket);
        MyConexionBT.start();
    }



    @Override
    protected void onPause() {
        super.onPause();
        try { // Cuando se sale de la aplicación esta parte permite
            // que no se deje abierto el socket
            btSocket.close();
        } catch (IOException e2) {
        }
    }

    //Comprueba que el dispositivo Bluetooth Bluetooth está disponible y solicita que se active si está desactivado
    private void VerificarEstadoBT() {

        if (btAdapter == null) {
            Toast.makeText(getBaseContext(), "El dispositivo no soporta bluetooth", Toast.LENGTH_LONG).show();
        } else {
            if (btAdapter.isEnabled()) {
            } else {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, 1);
            }
        }
    }

    //Crea la clase que permite crear el evento de conexion
    private class ConnectedThread extends Thread {
        private final InputStream mmInStream;

        public ConnectedThread(BluetoothSocket socket) {
            InputStream tmpIn = null;
            try {
                tmpIn = socket.getInputStream();
            } catch (IOException e) {
            }
            mmInStream = tmpIn;

        }

        public void run() {
            byte[] buffer = new byte[256];
            int bytes;

            // Se mantiene en modo escucha para determinar el ingreso de datos
            while (true) {
                try {
                    bytes = mmInStream.read(buffer);
                    String readMessage = new String(buffer, 0, bytes);
                    // Envia los datos obtenidos hacia el evento via handler
                    bluetoothIn.obtainMessage(handlerState, bytes, -1, readMessage).sendToTarget();

                }
                catch (IOException e)
                {
                }
            }
        }
    }


    private void MandarMedicion(String d, String c)
    {
        HashMap<String,String> hashMapToken = new HashMap<>();
        hashMapToken.put("usuario",d);
        hashMapToken.put("mediciones",c);

        JsonObjectRequest solicitud = new JsonObjectRequest(Request.Method.POST,IP_medicion,new JSONObject(hashMapToken), new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject datos) {
                try
                {
                    Toast.makeText(Graficar.this,datos.getString("resultado"),Toast.LENGTH_SHORT).show();
                    dato.setText("");
                }
                catch (JSONException e)
                {

                }

            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Toast.makeText(Graficar.this,"Ocurrio un error",Toast.LENGTH_SHORT).show();
            }
        });
        VolleyRP.addToQueue(solicitud,mRequest,this,volley);

    }


    private String getStringET(EditText e)
    {
        return e.getText().toString();
    }
}
