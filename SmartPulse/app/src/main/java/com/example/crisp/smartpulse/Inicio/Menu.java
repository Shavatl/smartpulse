package com.example.crisp.smartpulse.Inicio;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;

import com.example.crisp.smartpulse.BaseEstatica;
import com.example.crisp.smartpulse.Medio.Conexion;
import com.example.crisp.smartpulse.R;

public class Menu extends AppCompatActivity {

    CardView registro;
    CardView login;

    MediaPlayer mp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        registro = (CardView) findViewById(R.id.registro);
        login = (CardView) findViewById(R.id.login);

        mp = MediaPlayer.create(this, R.raw.click);

        if(BaseEstatica.obtenerBoleano(this,BaseEstatica.shared_estado))
        {
            Intent i = new Intent(this, Conexion.class);
            startActivity(i);
            finish();
        }

        registro.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent i = new Intent(Menu.this, Registro.class);
                startActivity(i);
                overridePendingTransition(R.anim.right_in,R.anim.right_out);
                mp.start();

            }
        });

        login.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent i = new Intent(Menu.this, Login.class);
                startActivity(i);
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
                mp.start();

            }
        });
    }
}
