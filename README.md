# SmartPulse

Aplicación móvil que recibe señales de un Arduino sobre el pulso de un usuario, contiene, interfaz para el usuario y al igual que una base de datos.

# Authors

* Salvador Quintero - (Developer, Designer) [[Gitlab](https://gitlab.com/Shavatl)]

## Others
```
Si tienes dudas o quieres contribuir con el proyecto o mejorarlo te puedes poner en contacto conmigo desde mi correo 
crispinquintero@gmail.co o por este medio, gracias.

```