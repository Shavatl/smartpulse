<?php
	
	require 'Database.php';

	class Registro{
		function _construct(){
		}

		public static function InsertarNuevoDato($id,$nombre,$apellido,$fecha,$telefono,$sangre,$peso,$altura,$genero){
			$consultar = "INSERT INTO DatosPersonales(id,nombre,apellidos,fecha_de_nacimiento,telefono,T_Sangre,Peso,Altura,genero) VALUES(?,?,?,?,?,?,?,?,?)";
			try{
				$resultado = Database::getInstance()->getDb()->prepare($consultar);
				return $resultado->execute(array($id,$nombre,$apellido,$fecha,$telefono,$sangre,$peso,$altura,$genero));
			}catch(PDOException $e){
				return false;
			}
		}

		public static function InsertarEnTablaLogin($id,$password){
			$consultar = "INSERT INTO Login(id,Password) VALUES(?,?)";
			try{
				$resultado = Database::getInstance()->getDb()->prepare($consultar);
				return $resultado->execute(array($id,$password));
			}catch(PDOException $e){
				return false;
			}
		}

	}

?>