<?php
	require 'Medicion.php';
	setlocale(LC_TIME, 'es_ME.UTF-8');
	date_default_timezone_set('America/Mexico_City');
	
	if($_SERVER['REQUEST_METHOD']=='POST')
	{
		$datos = json_decode(file_get_contents("php://input"),true);
		$usuario = $datos["usuario"];
		$mediciones = $datos["mediciones"];
		$NameTableUser = "Medicion_" . $usuario;

		$respuestaCreateTableUser = Medicion::CreateTable($NameTableUser);

		$fechaActual = getdate();
		$segundos = $fechaActual['seconds'];
		$minutos = $fechaActual['minutes'];
		$hora = $fechaActual['hours'];
		$dia = $fechaActual['mday'];
		$mes = $fechaActual['mon'];
		$year = $fechaActual['year'];

		$miliseconds = DateTime::createFromFormat('U.u',microtime(true));

		$id_user = $usuario . "_" . $hora . $minutos . $segundos . $miliseconds->format("u");

		$hora_medicion = strftime("%H:%M , %A, %d de %B de %Y");

		$respuestaEnviarMedicionUser = Medicion::EnviarMedicion($NameTableUser,$id_user,$mediciones,$hora_medicion);
		if($respuestaEnviarMedicionUser == 200)echo json_encode(array('resultado' => 'Guardado con exito'));
		else echo "No se pudo guardar";

	}

?>