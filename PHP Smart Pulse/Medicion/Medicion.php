<?php
	require 'Database.php';

	class Medicion{
		function _construct(){}
		
		public static function CreateTable($NameTable){
			try{
				$consultar = "CREATE TABLE $NameTable (
					id VARCHAR (50) PRIMARY KEY,
				 	presion VARCHAR(50) NOT NULL,
				  	fecha VARCHAR(50) NOT NULL )";
				$respuesta = Database::getInstance()->getDb()->prepare($consultar);
				$respuesta -> execute(array());
				return 200;
			}catch(PDOException $e){
				return -1;
			}
		}

		public static function EnviarMedicion($TableName, $id, $presion,$fecha){
			try{
				$consultar = "INSERT INTO $TableName (id,presion,fecha) VALUES(?,?,?)";
				$respuesta = Database::getInstance()->getDb()->prepare($consultar);
				$respuesta->execute(array($id,$presion,$fecha));
				return 200;
			}catch(PDOException $e){
				return -1;
			}
		}
	}

?>