<?php

	require 'Mediciones.php';

	if($_SERVER['REQUEST_METHOD']=='POST')
	{
		$datos = json_decode(file_get_contents("php://input"),true);
		$usuario = $datos["usuario"];
		$NameTableEmisor = "Medicion_" . $usuario;

		$respuesta = Medicion::solicitarMedicionUsuario($NameTableEmisor);
		if($respuesta)
		{
			echo json_encode(array("resultado"=>$respuesta));
		}else{
			echo json_encode(array("resultado"=>"No se pudo solicitar los mensajes"));
		}
	}
?>