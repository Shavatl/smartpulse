<?php
	require 'Database.php';

	class Medicion
	{
		function _construct()
		{
		}

		public static function solicitarMedicionUsuario($nameTable){
			try{
				$consultar = "SELECT * FROM $nameTable";
				$resultado = Database::getInstance()->getDb()->prepare($consultar);
				$resultado->execute();
				$tabla = $resultado->fetchAll(PDO::FETCH_ASSOC);
				return $tabla;
			}catch(PDOException $e){
				return false;
			}
		}

	}

?>